# Formation O'clock

- [Onboarding](docs/onboarding.md) 

- [Le programme officiel de la formation](docs/programme-officiel.md) (brut de décoffrage)

- [Remarques en vrac du formateur](docs/remarques-formateur.md)

- [Le programme revu et corrigé](docs/programme.md) (avec des pépites de cohérence pédagogique)

- [Brainstorming](docs/vrac.md) (idées en vrac)
