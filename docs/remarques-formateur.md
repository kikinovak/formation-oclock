# Formation O'clock - Remarques

> Le Lapin Blanc mit ses lunettes. "Plaise à Votre Majesté, où dois-je
> commencer?" demanda-t-il. "Commencez au commencement", dit le Roi d'un ton
> grave, "et continuez jusqu'à ce que vous arriviez à la fin; ensuite,
> arrêtez-vous."

Lewis Carroll, *Alice au pays des merveilles*

> Nous la raconterons en détail, exactement et minutieusement. En effet,
> l'intérêt d'une histoire ou l'ennui qu'elle nous cause ont-ils jamais dépendu
> de l'espace et du temps qu'elle a exigé? Sans craindre de nous exposer au
> reproche d'avoir été méticuleux à l'excès, nous inclinons au contraire à
> penser que seul est vraiment divertissant ce qui est minutieusement élaboré.

Thomas Mann, *La montagne magique*

> Grise, cher ami, est toute théorie \
> Et vert l'arbre doré de la vie.

Goethe, *Faust I*

---

Quelques remarques en vrac sur cette formation :

- Le [programme officiel de la formation tel qu'il a été réalisé par la
  Région](programme-officiel.md) est découpé en séquences qui laissent augurer
  un minimum de cohérence pédagogique, mais c'est loin d'être le cas. Au lieu
  de cela, il s'agit là d'une énumération pêle-mêle des différents points que
  l'on est susceptible d'aborder dans le cadre d'une formation Linux. D'un
  point de vue pédagogique, cette liste pleine de redondances n'a ni queue ni
  tête, et c'est au formateur de présenter tout cela de façon digeste et
  cohérente aux stagiaires.

- Il a vraisemblablement été élaboré par quelqu'un qui n'a jamais touché à
  l'administration d'un système Linux, ou alors par quelqu'un dont c'était le
  métier il y a fort longtemps. À titre d'exemple, il est question de "niveaux
  de démarrage", une notion qui n'est plus d'actualité dans les grandes
  distributions classiques (RHEL et dérivées, Debian, Ubuntu, SUSE, etc.)
  depuis une bonne dizaine d'années maintenant.

- Le programme donne le détail de certaines opérations comme les opérateurs de
  redirection. Mais dans ce cas, il aurait été logique de mentionner les autres
  opérations fondamentales comme le partitionnement, le formatage, le chargeur
  de démarrage, les processus, etc. Dans ce cas, on se demande pourquoi le
  document ne s'est pas appuyé sur une des nombreuses listes d'objectifs
  complètes existantes. Comme par exemple celle qui a été élaborée par un
  organisme officiel comme le Linux Professional Institute pour la
  [certification LPIC-1](https://wiki.lpi.org/wiki/LPIC-1_Objectives_V5.0(FR)).

- Conclusion : on va donc mettre un pied devant l'autre et avancer au rythme de
  chacun, en commençant par le début et en terminant par la fin. :o)
