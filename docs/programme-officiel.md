# Formation O'clock - Programme officiel

Voici le programme de la formation tel qu'il a été réalisé par la région.

## Séquence 1

- [ ] Installer et paramétrer un service sous Linux

- [ ] Connaissance des différentes branches de distributions et de la démarche
      de mise en production

- [ ] Utiliser les forums des communautés d’utilisateurs

- [ ] Mettre à jour la documentation technique

- [ ] Installer et paramétrer une distribution

- [ ] Effectuer les tâches d’administration en ligne de commande

- [ ] Configurer le réseau et le pare-feu sur le serveur

- [ ] Gérer le démarrage des services (niveau de démarrage)

- [ ] Configurer les services réseau (NTP, Mail, DHCP, DNS)

- [ ] Configurer les serveurs de bases de données au niveau système: MySQL,
      PostgreSQL

- [ ] Appliquer les recommandations de configuration d’un système Linux

## Séquence 2

- [ ] S’appuyer sur les communautés d’utilisateurs

- [ ] Connaissance de la philosophie des logiciels Open Source

- [ ] Connaissance de la déontologie des forums d’utilisateurs

- [ ] Identifier les communautés d’utilisateurs pertinentes

- [ ] Rechercher une information sur les forums d’utilisateurs

- [ ] Tester et valider les informations obtenues

- [ ] Publier un article sur les forums

- [ ] Exploiter une documentation technique ou une interface de logiciel en
      anglais

## Séquence 3

- [ ] Mettre une application en production

- [ ] Connaissance de la notion de container

- [ ] Connaissance des différentes architectures

- [ ] Connaissance du processus de mise en production (ITIL)

- [ ] Connaissance des différents environnements : tests, préproduction,
      production

- [ ] Installer une application LAMP

- [ ] Installer l’environnement Java

- [ ] Administrer et exploiter un serveur de données (MySQL, PostgreSQL)

- [ ] Écrire des requêtes simples en SQL

- [ ] Publier une application

- [ ] Utiliser les forums des communautés d’utilisateurs

## Séquence 4

- [ ] Développer des scripts d’automatisation

- [ ] Connaissance des bases de programmation

- [ ] Connaissance des opérateurs de redirection de Linux

- [ ] Connaissance des commandes de manipulation de texte de Linux

- [ ] Connaissance des risques liés à l’utilisation des scripts

- [ ] Créer des scripts shell

- [ ] Créer des scripts Python ou Perl

- [ ] Adapter des scripts

- [ ] Utiliser les forums des communautés d’utilisateurs

## Séquence 5

- [ ] Superviser les serveurs Linux

- [ ] Connaissance des principaux composants du système

- [ ] Connaissance des mécanismes d’allocation des ressources

- [ ] Mettre en place les agents de surveillance du serveur (SNMP, Syslog)

- [ ] Consulter les différents journaux d’événements

- [ ] Allouer des ressources supplémentaires

- [ ] Utiliser les forums des communautés d’utilisateurs

