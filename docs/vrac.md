# Brainstorming

Idées en vrac.

- Le métier de l'administrateur système

- Compléter la formation GitHub. C'est quelque chose qu'on pourra voir
  relativement tôt dans la formation, dès que les stagiaires auront appris à se
  servir de Vim et qu'ils maîtrisent les bases de la navigation dans le
  système, de créer et de supprimer des fichiers et des répertoires, etc. 

- Application pratique : créer sa propre documentation en texte simple au
  format HOWTO ou en Markdown.

- La virtualisation avec KVM : 

  * Mettre en place un hyperviseur (local et distant)

  * Installer une VM Rocky Linux 8

- Découvrir Vagrant

- Environnement labo local avec Vagrant

