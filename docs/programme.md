# Formation O'clock - Administration Systèmes Linux

Voici le programme de la formation revu et corrigé avec une bonne cohérence
pédagogique pour un public débutant.

## Culture générale : Unix, Linux, Open Source, logiciels libres

- [ ] [Les premiers ordinateurs et le projet Multics](https://blog.microlinux.fr/multics/)

- [ ] [De Multics à Unix](https://blog.microlinux.fr/unix/)

- [ ] [Richard Stallman vs. Xerox](https://blog.microlinux.fr/richard-stallman/)

- [ ] [Le projet GNU](https://blog.microlinux.fr/gnu/)

- [ ] [Linus Torvalds et Minix](https://blog.microlinux.fr/linus-torvalds-minix/)

- [ ] [Naissance du noyau Linux](https://blog.microlinux.fr/naissance-du-noyau-linux/)

- [ ] [Les premières distributions Linux](https://blog.microlinux.fr/premieres-distributions-linux/)

- [ ] [Slackware Linux](https://blog.microlinux.fr/slackware-linux/)

- [ ] [Red Hat, Fedora et CentOS](https://blog.microlinux.fr/redhat-fedora-centos/)

- [ ] [Debian](https://blog.microlinux.fr/debian/)

- [ ] [Ubuntu](https://blog.microlinux.fr/ubuntu/)

- [ ] [SUSE et OpenSUSE](https://blog.microlinux.fr/suse-opensuse/)

- [ ] [Les systèmes BSD](https://blog.microlinux.fr/systemes-bsd/)

- [ ] [Choisir sa distribution Linux](https://blog.microlinux.fr/choix-linux/)

- [ ] [Les distributions Linux pour l’entreprise](https://blog.microlinux.fr/distributions-linux-entreprise/)

- [ ] [Qui utilise Linux ?](https://blog.microlinux.fr/qui-utilise-linux/)

## Installer un système Linux pour s'entraîner

- [ ] [Installer un poste de travail Linux](https://blog.microlinux.fr/poste-de-travail-linux-formation/)

- [ ] [Installer VirtualBox](https://blog.microlinux.fr/virtualbox-formation/)

- [ ] [Installer Rocky Linux 8 dans VirtualBox](https://blog.microlinux.fr/install-rl8-virtualbox/)

- [ ] [Installer Rocky Linux 8 sur du matériel dédié](https://blog.microlinux.fr/install-rl8-materiel/)

- [ ] [Se connecter à un serveur Linux](https://blog.microlinux.fr/connexion-serveur-linux/)

- [ ] [Linux, Shakespeare et Molière](https://blog.microlinux.fr/linux-i18n/)

## Fondamentaux du shell

- [ ] [Naviguer en mode texte](https://blog.microlinux.fr/naviguer/)

- [ ] [Les commandes de sortie](https://blog.microlinux.fr/les-commandes-de-sortie/)

- [ ] [La structure des répertoires](https://blog.microlinux.fr/structure/)

- [ ] [Visualiser : `more` et `less`](https://blog.microlinux.fr/visualiser/)

- [ ] [Créer : `touch` et `mkdir`](https://blog.microlinux.fr/creer/)

- [ ] [Copier, déplacer et renommer : `cp` et `mv`](https://blog.microlinux.fr/copier/)

- [ ] [Supprimer : `rm` et `rmdir`](https://blog.microlinux.fr/supprimer/)

- [ ] [Éditer des fichiers texte : Vi](https://blog.microlinux.fr/vim/)

- [ ] [Travailler efficacement dans le shell](https://blog.microlinux.fr/travailler-efficacement/)

- [ ] [Consulter l’aide en ligne : `man` et `info`](https://blog.microlinux.fr/rtfm/)

## Gestion de versions avec Git

- [ ] [Présentation de Git](https://blog.microlinux.fr/formation-git-01-presentation/)

- [ ] [Installation & Configuration](https://blog.microlinux.fr/formation-git-02-installation-configuration/)

- [ ] [Accéder à GitHub et GitLab](https://blog.microlinux.fr/formation-git-03-acces-github-gitlab/)

- [ ] [Initialiser un dépôt Git](https://blog.microlinux.fr/formation-git-04-initialiser/)

- [ ] [Les fichiers dans tous leurs états](https://blog.microlinux.fr/formation-git-05-fichiers-etats/)

- [ ] [État des lieux](https://blog.microlinux.fr/formation-git-06-etat-des-lieux/)

- [ ] [Les idées fusent](https://blog.microlinux.fr/formation-git-07-idees-fusent/)

- [ ] [Utiliser les branches](https://blog.microlinux.fr/formation-git-08-branches/)

- [ ] [Le fast-forward merge](https://blog.microlinux.fr/formation-git-09-fast-forward-merge/)

- [ ] [Le merge commit](https://blog.microlinux.fr/formation-git-10-merge-commit/)

- [ ] [Gérer les conflits](https://blog.microlinux.fr/formation-git-11-conflits/)

- [ ] [Afficher l’historique](https://blog.microlinux.fr/formation-git-12-logs/)

- [ ] [Afficher les modifications](https://blog.microlinux.fr/formation-git-13-diff/)

- [ ] [Rectifier le tir](https://blog.microlinux.fr/formation-git-14-restore/)

- [ ] [Supprimer des fichiers](https://blog.microlinux.fr/formation-git-15-supprimer/)

- [ ] [Renommer des fichiers](https://blog.microlinux.fr/formation-git-16-renommer/)

- [ ] [Renommer des branches](https://blog.microlinux.fr/formation-git-17-renommer-branches/)

- [ ] [Prise de tête avec HEAD](https://blog.microlinux.fr/formation-git-18-head/)

- [ ] [Retour vers le commit](https://blog.microlinux.fr/formation-git-19-retour-commit/)

- [ ] [Cloner un dépôt](https://blog.microlinux.fr/formation-git-20-clone/)

- [ ] [Publier vos commits](https://blog.microlinux.fr/formation-git-21-push/)

- [ ] [Gare à ce que vous publiez !](https://blog.microlinux.fr/formation-git-22-remote/)

- [ ] [La fusion revisitée](https://blog.microlinux.fr/formation-git-23-merge/)

- [ ] [Le pull request](https://blog.microlinux.fr/formation-git-24-pull-request/)

## Fondamentaux de l'administration système

- [ ] [Gérer les utilisateurs](https://blog.microlinux.fr/utilisateurs/)

- [ ] [Gérer les droits d’accès](https://blog.microlinux.fr/droits/)

- [ ] [Les outils de recherche](https://blog.microlinux.fr/recherche/)

- [ ] [Créer et manipuler des liens](https://blog.microlinux.fr/liens/)

- [ ] [Gérer les processus](https://blog.microlinux.fr/processus/)

- [ ] [Gérer les services](https://blog.microlinux.fr/services/)

- [ ] [Accéder aux périphériques amovibles](https://blog.microlinux.fr/peripheriques/)

- [ ] [Partitionner et formater un disque dur](https://blog.microlinux.fr/partitionnement/)

- [ ] [Gérer les archives compressées](https://blog.microlinux.fr/archives/)

- [ ] [Installer des logiciels depuis le code source](https://blog.microlinux.fr/logiciels-source/)

- [ ] [Utiliser le gestionnaire de paquets RPM](https://blog.microlinux.fr/logiciels-rpm/)

- [ ] [Utiliser le gestionnaire de paquets DNF](https://blog.microlinux.fr/logiciels-dnf/)

- [ ] [Premiers pas sur le réseau](https://blog.microlinux.fr/reseau)

- [ ] Manipuler le chargeur de démarrage et le noyau

- [ ] Installer et gérer un RAID logiciel

- [ ] Gérer les volumes logiques avec LVM

- [ ] Gérer les logs du système

- [ ] Un système de base aux petits oignons

- [ ] L'authentification par clé SSH

- [ ] Je vous sers quelque chose ?

- [ ] Un pare-feu fait maison

- [ ] SELinux expliqué aux administrateurs frileux

- [ ] Transformer un serveur en routeur

- [ ] Installer Rocky Linux sur un serveur dédié

- [ ] Gérer les attaques par force brute

- [ ] Synchroniser vos machines avec NTP

- [ ] Au secours ! Mon serveur ne démarre plus !

- [ ] Configurer Postfix pour l'envoi de courriels

- [ ] Configurer les mises à jour automatiques

- [ ] Installer un serveur DNS avec BIND

- [ ] Installer un serveur de bases de données MariaDB

- [ ] Installer un serveur web Apache

- [ ] Obtenir et gérer des certificats SSL/TLS gratuits

- [ ] Hébergement sécurisé avec Apache et SSL

- [ ] Hébergement WordPress

- [ ] Hébergement Dolibarr

- [ ] Héberger un stockage réseau avec OwnCloud
