# Formation O'clock - Onboarding

Les outils et process internes utilisés par O'Clock

## Objectif

Rendre la formation à distance encore plus humaine et engageante que les
meilleures formations présentielles.

## Compte `@oclock.io`

Le compte `@oclock.io` est le point d'entrée de tous les systèmes en interne :

[Google](https://www.google.fr) > Connexion > `prenom.nom.ext@oclock.io`

La double authentification doit **impérativement** être activée :

Gérer votre compte Google > Sécurité > Validation en deux étapes > Activation

À partir de là, l'adresse `prenom.nom.ext@oclock.io` peut être configurée dans
Mozilla Thunderbird. L'essentiel de la communication mail de O'Clock passera
par cette adresse.

Tester la connexion SSO (Single Sign-On) :

  * [Kourou](https://kourou.oclock.io) > Se connecter via Keycloak

  * [Slippers](https://oclock.slippers.live) > O'Clock Team > *Sign in*

## Slack

Slack est le principal outil de communication chez O'Clock. Il vaut mieux
installer le client lourd pour une meilleure gestion des notifications.

### L'espace de travail O'Clock Admin

L'espace de travail O'Clock Admin permet à tous les salariés et partenaires
O'Clock de communiquer. Il n'est pas accessible pour les apprenants.

Dans Slack, un espace de travail regroupe des canaux et des personnes.

En tant que *freelance*, on a besoin de connaître la fonction de cinq canaux :

- `#peda-freelance` : canal public auquel ont accès tous les *freelances* et
  qui permet d'interagir avec les collègues `freelances`. Éviter de poser des
  questions sensibles (contrat, incident, etc.) sur ce canal.

- `#support-formateur-urgence` : canal qui permet de solliciter le pôle
  technique en cas d'incident technique majeur (VM Cloud en panne, bug dans
  Slippers, etc.)

- `#suivi-missions-prenom-nom` : canal privé auquel ont accès la directrice
  pédagogique, le responsable des programmes, l'assistante administrative et
  tous les managers de formateurs. Ce canal permet de se mettre en relation
  avec le haut de la pyramide hiérarchique pédagogique O'Clock. Utile pour
  toute question sensible, une demande spécifique au contrat et la mission,
  etc.

- `#peda-promo-<promo>` : permet d'interagir avec tous les acteurs qui
  interviennent sur cette promotion : formateurs, tuteurs, CPA, etc.

- `#peda-promo-<promo>-ateliers` : permet de suivre l'activité des élèves pour
  pouvoir les aider en cas de difficulté dans le contexte des exercices en
  autonomie.

> Un CPA (*Conseiller Parcours Apprenant*) est une personne qui accompagne les
> apprenants en cas de difficultés péripédagogiques (santé, social, baisse de
> moral, comportement, etc.

**Important** : sur les canaux `#peda-promo-<promo>` et
`#peda-promo-<promo>-ateliers` il faut modifier le paramétrage des
notifications pour être averti de **tous** les messages.

### L'espace de travail O'Clock

Cet espace de travail est dédié aux admissions. C'est par là qu'arrivent les
apprenants. Lorsqu'une promotion démarre sa Saison 00, le tuteur les accueille
sur O'Clock, il leur envoie leurs identifiants de connexion, etc.

### L'espace de travail O'Clock Promotion

Tous les apprenants de la promotion ont accès à cet espace de travail. Ils
échangent entre eux sur plusieurs canaux :

- `#general` : toute la communication générale pour la promotion. C'est ici
  qu'on pourra venir se présenter à la promotion avant de prendre la mission,
  par exemple.

- `#entraide` : canal sur lequel les apprenants peuvent s'entraider pendant les
  exercices qu'ils doivent réaliser en autonomie. Il est très important
  d'inviter la promotion à s'aider le plus possible du côté de ce canal et de
  le solliciter lorsqu'on rencontre des difficultés pour renforcer la cohésion
  du groupe.

- `#sav` : canal sur lequel les apprenants peuvent solliciter le pôle technique
  pour remonter des éventuels problèmes techniques.

- `#soutien-ateliers` : canal sur lequel les apprenants peuvent demander de
  l'aide pendant un atelier.

## Google Drive

[Google Drive](https://drive.google.com) > Partagés avec moi > Pédagogie >
Suivi Promos > Nom de la promotion.

Trois éléments à retenir :

- Le dossier **Partage apprenants**

- Le fichier **Planning <promotion>**

- Le fichier **Suivi pédagogique**

Les *replays* devront être uploadés en fin de journée dans le dossier **Partage
apprenants**, dans le dossier **replays**.

Le fichier **Suivi pédagogique** est organisé en plusieurs onglets. Ce n'est
pas la peine de remplir chacune des cellules pour chaque journée. Cependant on
gardera une trace lorsqu'un élève a un comportement déplacé durant le cours,
lorsqu'un élève fantôme se met à interagir fortement dans le tchat, ou alors
lorsqu'un élève moteur qui participe beaucoup ne poste plus aucun message, etc.

## Slippers

[Slippers](https://oclock.slippers.live) est le système développé en interne
par O'Clock qui permet de dispenser les cours auprès des apprenants.

**Attention !** Slippers fonctionne uniquement sous Chrome. Actuellement
l'audio ne fonctionne pas avec Firefox et Chromium.

> Avant mars 2023, O'Clock utilisait la plateforme Cockpit.

La page d'accueil affiche la liste des cours actuellement dispensés :

- Titre du cours

- Nom du formateur

- Nom du *helper* (tuteur)

- Le bouton pour rejoindre le cours

> L'icône **Ghost** sous forme de fantôme Pacman permet de rejoindre la classe
  en mode fantôme. Personne ne pourra voir que l'on est dans la classe, sauf si
  l'on se manifeste dans le tchat.

Créer une salle de classe : Administration > Cours > Nouveau cours :

- Titre du cours

- Promotion

- Formateur

- Date du cours

- Helpers

- Durée de la séance : 08h45 à 17h00

> Important : une fois qu'on a créé un cours, il ne faut jamais (!) modifier la
> date du cours a posteriori, même si Slippers le permet. Ça fait planter
> l'application.

Cliquer sur **Enregistrer**. Le cours apparaît alors dans la liste des cours
sur la page d'accueil de Slippers.

La ligne du cours sur la page d'accueil arbore une icône **Formateur** qui
indique que je peux rejoindre le cours en tant que professeur.

## OBS Studio

[OBS Studio](https://obsproject.com) (*Open Broadcaster Software Studio*) est
un logiciel *open source* conçu pour la diffusion en direct et l'enregistrement
de vidéos. Chez O'Clock, tous les cours sont enregistrés grâce à OBS et mis à
disposition des apprenants.

Sous Rocky Linux 8, le dépôt tiers RPM Fusion Free fournit le paquet binaire de
l'application :

```
# dnf install -y obs-studio
```

### Configuration initiale

La configuration est stockés dans `~/.config/obs-studio`.

Passer l'assistant de configuration au démarrage.

**Profil** > **Nouveau** : O'Clock

Désactiver l'assistant de configuration automatique.

**Scènes** > **Scène** > Clic droit > **Renommer** : Slippers

**Sources** > `+` > **Capture d'écran (XSHM)** > **Créer une nouvelle source**

Sélectionner l'écran à capturer dans le menu déroulant.

Garder l'option **Capturer le curseur**.

**Sources** > `+` > **Capture audio (Entrée PulseAudio)** > **Périphérique** >
Choisir le micro > **OK**

**Paramètres** > **Vidéo** > vérifier si la **Résolution de Sortie** est
identique à la **Résolution de base** pour éviter de recalculer à chaud la
résolution de la vidéo.

**Débit d'images** : 30 FPS

**Paramètres** > **Sortie** > **Encodeur** : Matériel (NVENC)

**Chemin d'accès de l'enregistrement** : `~/Vidéos`

**Qualité d'enregistrement** : Haute qualité, taille de fichier moyenne.
La définition explicite de la qualité permettra de mettre en pause une vidéo.

**Format d'enregistrement** : `mkv`

Dans la fenêtre principale, repérer le **Mélangeur audio** et désactiver
l'Audio du Bureau en cliquant sur l'icône du haut-parleur qui doit apparaître
en mode silencieux (*Mute*).

## Kourou

Une fois que le cours est terminé, se connecter à
[Kourou](https://kourou.oclock.io/). 

> L'application est basée sur WordPress.

**Barre d'outils** > **Créer** > **Récapitulatifs quotidiens** > **Nouveau
récap** :

Renseigner les infos sommaires en sélectionnant **Mono cockpit**.

Remplir la page selon le *template* sans oublier de renseigner le titre.

Sélectionner la saison dans la barre latérale droite.

Une fois qu'on a publié la page, vérifier si l'info a bien été propagée sur
Slack dans le canal `#challenges`.





